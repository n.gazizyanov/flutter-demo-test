import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_quill/flutter_quill.dart';
import 'package:flutter_quill/models/rules/format.dart';
import 'package:flutter_quill/models/rules/insert.dart';
import 'package:tuple/tuple.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          body: SafeArea(
              child: _Content()
          )
      ),
    );
  }
}

class _Content extends StatefulWidget {
  const _Content({
    Key? key,
  }) : super(key: key);

  @override
  State<_Content> createState() => _ContentState();
}

class _ContentState extends State<_Content> {
  final QuillController controller = QuillController(
    document: Document()..setCustomRules([CustomRule()]),
    selection: const TextSelection.collapsed(offset: 0),
  );

  @override
  void initState() {
    controller.addListener(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(
          children: [
            // QuillToolbar.basic(controller: controller),
            Expanded(
              child: QuillEditor(
                controller: controller,
                readOnly: false,
                scrollController: ScrollController(),
                scrollable: true,
                focusNode: FocusNode(),
                autoFocus: true,
                expands: false,
                padding: EdgeInsets.zero,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CustomRule extends InsertRule {
  const CustomRule();

  @override
  Delta? applyRule(Delta document, int index, {int? len, Object? data, Attribute? attribute}) {
    
    final itr = DeltaIterator(document);
    final prev = itr.skip(index);
    if (prev == null || prev.data is! String) {
      return null;
    }

    try {
      final cand = (prev.data as String).split('\n').last;
      final attributes = prev.attributes ?? <String, dynamic>{};

      if (!['игил'].contains(cand.toLowerCase())) {
        if (attributes.containsKey(Attribute.link.key)) {
          attributes.remove(Attribute.link.key);
          debugPrint('gg');
          return Delta()
            ..retain(index + (len ?? 0) - cand.length)
            ..retain(cand.length, attributes)
            ..insert(data, attributes);
        }
        return null;
    }

      if (attributes.containsKey(Attribute.link.key)) {
        return null;
      }

      attributes.addAll(LinkAttribute(cand.toString()).toJson());
      return Delta()
        ..retain(index + (len ?? 0) - cand.length)
        ..retain(cand.length, attributes)
        ..insert(data, prev.attributes);
    } on FormatException {
      return null;
    }
  }
}